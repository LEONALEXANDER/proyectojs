import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { RolesService } from './roles.service';
import { CreateRolDto } from './dto/create-roles.tdo';
import { HasRoles } from '../auth/jwt/has-roles';
import { JwtRole } from 'src/auth/jwt/jwt-rol';
import { JwtAuthGuard } from 'src/auth/jwt/jwt-auth.guard';
import { JwtRolesGuard } from 'src/auth/jwt/jwt-roles.guard';
import { Rol } from './roles.entity';

@Controller('roles')
export class RolesController {

    constructor(private readonly rolesService: RolesService) {}

    @HasRoles(JwtRole.ADMIN)
    @UseGuards(JwtAuthGuard,JwtRolesGuard)
    @Post()
    create(@Body() rol:CreateRolDto): Promise<Rol>{
        return this.rolesService.create(rol);
    }
}
