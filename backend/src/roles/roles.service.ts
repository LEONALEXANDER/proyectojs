import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Rol } from './roles.entity';
import { Repository } from 'typeorm';
import { CreateRolDto } from './dto/create-roles.tdo';

@Injectable()
export class RolesService {

    constructor(@InjectRepository(Rol) private rolesRepository:Repository<Rol>){}

    create(rol:CreateRolDto){
        const newRol = this.rolesRepository.create(rol);
        return this.rolesRepository.save(newRol);
    }
    
    findAll(){
        return this.rolesRepository.find();
    }
    
    // findOne(id:str){
    //     return this.rolesRepository.findOne(id);
    // }

}
