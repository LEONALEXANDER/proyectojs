import { IsEmail, IsString } from "class-validator";

export class UpdateUserDto{
    name?:string;
    lastname?:string;
    email?:string;
    phone?:string;
    image?:string;

}